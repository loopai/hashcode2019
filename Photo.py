class Photo:
    def __init__(self, id:int, tags: set(), vertical: str):
        self.id: int = id
        self.tags: set() = tags
        self.vertical: str = vertical
        self.is_vertical: bool = True if self.vertical == 'V' else False

    def intersection(self, other: 'Photo'):
        return self.tags.intersection(other.tags)

